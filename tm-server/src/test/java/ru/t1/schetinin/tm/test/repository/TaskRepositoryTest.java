package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.ITaskRepository;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.TaskRepository;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.ConnectionService;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.test.service.PropertyServiceTest;
import ru.t1.schetinin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    public final static Project PROJECT_TEST1 = new Project();

    @NotNull
    public final static Task TASK_TEST1 = new Task();

    @NotNull
    public final static Task TASK_TEST2 = new Task();

    @NotNull
    public final static Task TASK_TEST3 = new Task();

    @NotNull
    public final static String TASK_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final Connection CONNECTION = CONNECTION_SERVICE.getConnection();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository(CONNECTION);

    @NotNull
    private final ITaskRepository TASK_REPOSITORY = new TaskRepository(CONNECTION);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
        PROJECT_TEST1.setName("Project_task_1");
        PROJECT_TEST1.setDescription("description_1");
        TASK_TEST1.setName("Test_task_1");
        TASK_TEST1.setDescription("description_1");
        TASK_TEST1.setProjectId(PROJECT_TEST1.getId());
        TASK_TEST2.setName("Test_task_2");
        TASK_TEST2.setDescription("description_2");
        TASK_TEST2.setProjectId(PROJECT_TEST1.getId());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = USER_REPOSITORY.findByLogin("test_login");
        if (user != null) USER_REPOSITORY.remove(user);
    }

    @Before
    public void initDemoData() throws Exception {
        PROJECT_REPOSITORY.add(USER_ID, PROJECT_TEST1);
        TASK_REPOSITORY.add(USER_ID, TASK_TEST1);
        TASK_REPOSITORY.add(USER_ID, TASK_TEST2);
    }

    @After
    public void clearData() throws Exception {
        TASK_REPOSITORY.clear(USER_ID);
        PROJECT_REPOSITORY.clear(USER_ID);
    }

    @Test
    public void testAddTask() throws Exception {
        Assert.assertNotNull(TASK_REPOSITORY.add(USER_ID, TASK_TEST3));
        @Nullable final Task task = TASK_REPOSITORY.findOneById(USER_ID, TASK_TEST3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_TEST3.getId(), task.getId());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final Task task = TASK_REPOSITORY.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertTrue(TASK_REPOSITORY.existsById(task.getId()));
        Assert.assertTrue(TASK_REPOSITORY.existsById(task.getUserId(), task.getId()));
        Assert.assertFalse(TASK_REPOSITORY.existsById(TASK_ID_FAKE));
    }

    @Test
    public void testCreateTask() throws Exception {
        @NotNull final Task task = TASK_REPOSITORY.create(USER_ID, TASK_TEST3.getName());
        Assert.assertEquals(TASK_TEST3.getName(), task.getName());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void testCreateTaskWithDesc() throws Exception {
        @NotNull final Task task = TASK_REPOSITORY.create(USER_ID, TASK_TEST3.getName(), TASK_TEST3.getDescription());
        Assert.assertEquals(TASK_TEST3.getName(), task.getName());
        Assert.assertEquals(TASK_TEST3.getDescription(), task.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(Collections.emptyList(), TASK_REPOSITORY.findAll(""));
        final List<Task> tasks = TASK_REPOSITORY.findAll(USER_ID);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void testFindAllSort() throws Exception {
        @NotNull final String sortType = "BY_CREATED";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Task> taskSort  = TASK_REPOSITORY.findAll(USER_ID, sort.getComparator());
        Assert.assertNotNull(TASK_REPOSITORY.findAll(sort.getComparator()).toString());
        Assert.assertEquals(TASK_REPOSITORY.findAll(USER_ID).toString(), taskSort.toString());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<Task> tasks = TASK_REPOSITORY.findAll(USER_ID);
        @NotNull final Task task1 = tasks.get(0);
        @NotNull final String taskId = task1.getId();
        Assert.assertEquals(task1.toString(), TASK_REPOSITORY.findOneById(taskId).toString());
    }

    @Test
    public void testClearUser() throws Exception {
        TASK_REPOSITORY.clear(USER_ID);
        Assert.assertEquals(0, TASK_REPOSITORY.getSize(USER_ID));
    }

    @Test
    public void testRemove() throws Exception {
        @Nullable final Task removeTask = TASK_REPOSITORY.remove(TASK_TEST2);
        Assert.assertNotNull(removeTask);
        Assert.assertEquals(TASK_TEST2.getId(), removeTask.getId());
        Assert.assertNull(TASK_REPOSITORY.findOneById(removeTask.getId()));
    }

    @Test
    public void testRemoveUser() throws Exception {
        @Nullable final Task removeTask = TASK_REPOSITORY.remove(USER_ID, TASK_TEST2);
        Assert.assertNotNull(removeTask);
        Assert.assertEquals(TASK_TEST2.getId(), removeTask.getId());
        Assert.assertNull(TASK_REPOSITORY.findOneById(removeTask.getId()));
    }

    @Test
    public void testRemoveById() throws Exception {
        @Nullable final Task removeTask = TASK_REPOSITORY.removeById(TASK_TEST2.getId());
        Assert.assertNotNull(removeTask);
        Assert.assertEquals(TASK_TEST2.getId(), removeTask.getId());
        Assert.assertNull(TASK_REPOSITORY.findOneById(removeTask.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        @Nullable final Task removeTask = TASK_REPOSITORY.remove(USER_ID, TASK_TEST2);
        Assert.assertNull(TASK_REPOSITORY.removeById(USER_ID, null));
        Assert.assertNull(TASK_REPOSITORY.removeById("USER_ID_FAKE", removeTask.getId()));
    }

    @Test
    public void testTaskFindAllByProjectId() throws Exception {
        @NotNull final List<Task> tasks = TASK_REPOSITORY.findAllByProjectId(USER_ID, PROJECT_TEST1.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void update() throws Exception {
        TASK_TEST1.setName(TASK_TEST3.getName());
        TASK_REPOSITORY.update(TASK_TEST1);
        Assert.assertEquals(TASK_TEST3.getName(), TASK_REPOSITORY.findOneById(USER_ID, TASK_TEST1.getId()).getName());
    }

}