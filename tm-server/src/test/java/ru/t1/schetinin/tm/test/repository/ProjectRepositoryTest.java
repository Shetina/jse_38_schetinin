package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.ConnectionService;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.test.service.PropertyServiceTest;
import ru.t1.schetinin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.*;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    public final static Project PROJECT_TEST1 = new Project();

    @NotNull
    public final static Project PROJECT_TEST2 = new Project();

    @NotNull
    public final static Project PROJECT_TEST3 = new Project();

    @NotNull
    public final static String PROJECT_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final Connection CONNECTION = CONNECTION_SERVICE.getConnection();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository(CONNECTION);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
        PROJECT_TEST1.setName("Test_project_1");
        PROJECT_TEST1.setDescription("description_1");
        PROJECT_TEST2.setName("Test_project_2");
        PROJECT_TEST2.setDescription("description_2");
        PROJECT_TEST3.setName("Test_project_3");
        PROJECT_TEST3.setDescription("description_3");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = USER_REPOSITORY.findByLogin("test_login");
        if (user != null) USER_REPOSITORY.remove(user);
    }


    @Before
    public void initDemoData() throws Exception {
        PROJECT_REPOSITORY.add(USER_ID, PROJECT_TEST1);
        PROJECT_REPOSITORY.add(USER_ID, PROJECT_TEST2);
    }

    @After
    public void clearData() throws Exception {
        PROJECT_REPOSITORY.clear(USER_ID);
    }

    @Test
    public void testAddProject() throws Exception {
        Assert.assertNotNull(PROJECT_REPOSITORY.add(USER_ID, PROJECT_TEST3));
        @Nullable final Project project = PROJECT_REPOSITORY.findOneById(USER_ID, PROJECT_TEST3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_TEST3.getId(), project.getId());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final Project project = PROJECT_REPOSITORY.findOneById(USER_ID, PROJECT_TEST2.getId());
        Assert.assertTrue(PROJECT_REPOSITORY.existsById(project.getId()));
        Assert.assertTrue(PROJECT_REPOSITORY.existsById(project.getUserId(), project.getId()));
        Assert.assertFalse(PROJECT_REPOSITORY.existsById(PROJECT_ID_FAKE));
    }

    @Test
    public void testCreateProject() throws Exception {
        @NotNull final Project project = PROJECT_REPOSITORY.create(USER_ID, PROJECT_TEST3.getName());
        Assert.assertEquals(PROJECT_TEST3.getName(), project.getName());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void testCreateProjectWithDesc() throws Exception {
        @NotNull final Project project = PROJECT_REPOSITORY.create(USER_ID, PROJECT_TEST3.getName(), PROJECT_TEST3.getDescription());
        Assert.assertEquals(PROJECT_TEST3.getName(), project.getName());
        Assert.assertEquals(PROJECT_TEST3.getDescription(), project.getDescription());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(Collections.emptyList(), PROJECT_REPOSITORY.findAll(""));
        final List<Project> projects = PROJECT_REPOSITORY.findAll(USER_ID);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void testFindAllSort() throws Exception {
        @NotNull final String sortType = "BY_CREATED";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Project> projectSort = PROJECT_REPOSITORY.findAll(USER_ID, sort.getComparator());
        Assert.assertNotNull(PROJECT_REPOSITORY.findAll(sort.getComparator()).toString());
        Assert.assertEquals(PROJECT_REPOSITORY.findAll(USER_ID).toString(), projectSort.toString());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<Project> projects = PROJECT_REPOSITORY.findAll(USER_ID);
        @NotNull final Project project1 = projects.get(0);
        @NotNull final String projectId = project1.getId();
        Assert.assertEquals(project1.toString(), PROJECT_REPOSITORY.findOneById(projectId).toString());
    }

    @Test
    public void testClearUser() throws Exception {
        PROJECT_REPOSITORY.clear(USER_ID);
        Assert.assertEquals(0, PROJECT_REPOSITORY.getSize(USER_ID));
    }

    @Test
    public void testRemove() throws Exception {
        @Nullable final Project removeProject = PROJECT_REPOSITORY.remove(PROJECT_TEST2);
        Assert.assertNotNull(removeProject);
        Assert.assertEquals(PROJECT_TEST2.getId(), removeProject.getId());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(removeProject.getId()));

    }

    @Test
    public void testRemoveUser() throws Exception {
        @Nullable final Project removeProject = PROJECT_REPOSITORY.remove(USER_ID, PROJECT_TEST2);
        Assert.assertNotNull(removeProject);
        Assert.assertEquals(PROJECT_TEST2.getId(), removeProject.getId());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(removeProject.getId()));
    }

    @Test
    public void testRemoveById() throws Exception {
        @Nullable final Project removeProject = PROJECT_REPOSITORY.removeById(PROJECT_TEST2.getId());
        Assert.assertNotNull(removeProject);
        Assert.assertEquals(PROJECT_TEST2.getId(), removeProject.getId());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(removeProject.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        @Nullable final Project removeProject = PROJECT_REPOSITORY.remove(USER_ID, PROJECT_TEST2);
        Assert.assertNull(PROJECT_REPOSITORY.removeById(USER_ID, null));
        Assert.assertNull(PROJECT_REPOSITORY.removeById("USER_ID_FAKE", removeProject.getId()));
    }

    @Test
    public void update() throws Exception {
        PROJECT_TEST1.setName(PROJECT_TEST3.getName());
        PROJECT_REPOSITORY.update(PROJECT_TEST1);
        Assert.assertEquals(PROJECT_TEST3.getName(), PROJECT_REPOSITORY.findOneById(USER_ID, PROJECT_TEST1.getId()).getName());
    }

}
