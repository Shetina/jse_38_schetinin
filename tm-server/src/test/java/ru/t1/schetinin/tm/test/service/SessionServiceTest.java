package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.api.service.ISessionService;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.ConnectionService;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.service.SessionService;
import ru.t1.schetinin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    public final static Session SESSION_TEST1 = new Session();

    @NotNull
    public final static Session SESSION_TEST2 = new Session();

    @NotNull
    public final static Session SESSION_TEST3 = new Session();

    @NotNull
    public final static String SESSION_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final Connection CONNECTION = CONNECTION_SERVICE.getConnection();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION);

    @NotNull
    private final ISessionService SESSION_SERVICE = new SessionService(CONNECTION_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
        SESSION_TEST1.setRole(user.getRole());
        SESSION_TEST2.setRole(user.getRole());
        SESSION_TEST3.setRole(user.getRole());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = USER_REPOSITORY.findByLogin("test_login");
        if (user != null) USER_REPOSITORY.remove(user);
    }

    @Before
    public void initDemoData() throws Exception {
        SESSION_SERVICE.add(USER_ID, SESSION_TEST1);
        SESSION_SERVICE.add(USER_ID, SESSION_TEST2);
    }

    @After
    public void clearData() throws Exception {
        SESSION_SERVICE.clear(USER_ID);
    }

    @Test
    public void testAddSession() throws Exception {
        Assert.assertNotNull(SESSION_SERVICE.add(USER_ID, SESSION_TEST3));
        @Nullable final Session session = SESSION_SERVICE.findOneById(USER_ID, SESSION_TEST3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_TEST3.getId(), session.getId());
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final Session session = SESSION_SERVICE.findOneById(USER_ID, SESSION_TEST1.getId());
        Assert.assertTrue(SESSION_SERVICE.existsById(session.getId()));
        Assert.assertTrue(SESSION_SERVICE.existsById(session.getUserId(), session.getId()));
        Assert.assertFalse(SESSION_SERVICE.existsById(SESSION_ID_FAKE));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Session> sessions = SESSION_SERVICE.findAll();
        @NotNull final List<Session> sessions2 = SESSION_SERVICE.findAll(USER_ID);
        Assert.assertEquals(sessions.size(), SESSION_SERVICE.getSize());
        Assert.assertEquals(sessions2.size(), SESSION_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<Session> sessions = SESSION_SERVICE.findAll(USER_ID);
        @NotNull final Session sessions1 = sessions.get(0);
        @NotNull final String sessionsId = sessions1.getId();
        Assert.assertEquals(sessions1.getId(), SESSION_SERVICE.findOneById(sessionsId).getId());
    }

    @Test
    public void testClearUser() throws Exception {
        SESSION_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SESSION_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testRemove() throws Exception {
        @Nullable final Session sessionRemove = SESSION_SERVICE.remove(SESSION_TEST2);
        Assert.assertNotNull(sessionRemove);
        Assert.assertEquals(SESSION_TEST2.getId(), sessionRemove.getId());
        Assert.assertNull(SESSION_SERVICE.findOneById(sessionRemove.getId()));
    }

    @Test
    public void testRemoveUser() throws Exception {
        @Nullable final Session removedSession = SESSION_SERVICE.remove(USER_ID, SESSION_TEST2);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(SESSION_TEST2.getId(), removedSession.getId());
        Assert.assertNull(SESSION_SERVICE.findOneById(removedSession.getId()));
    }

    @Test
    public void testRemoveById() throws Exception {
        @Nullable final Session removedSession = SESSION_SERVICE.removeById(SESSION_TEST2.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(SESSION_TEST2.getId(), removedSession.getId());
        Assert.assertNull(SESSION_SERVICE.findOneById(removedSession.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        @Nullable final Session removedSession = SESSION_SERVICE.remove(USER_ID, SESSION_TEST2);
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(USER_ID, null));
        Assert.assertNull(SESSION_SERVICE.removeById("USER_ID_FAKE", removedSession.getId()));
    }

    @Test
    public void testRemoveAll() throws Exception {
        @NotNull final List<Session> sessions = SESSION_SERVICE.findAll(USER_ID);
        SESSION_SERVICE.removeAll(sessions);
        Assert.assertEquals(0, SESSION_SERVICE.findAll(USER_ID).size());
    }

}