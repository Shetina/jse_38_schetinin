package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.ConnectionService;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    public final static String USER_TEST_LOGIN = "USER_TEST_LOGIN";

    @NotNull
    public final static String USER_TEST_PASSWORD = "USER_TEST_PASSWORD";

    @NotNull
    public final static String USER_TEST_EMAIL = "USER_TEST@MAIL";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "ADMIN_TEST_LOGIN";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "ADMIN_TEST_PASSWORD";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "ADMIN_TEST@MAIL";

    @NotNull
    public final static User USER_TEST = new User();

    @NotNull
    public final static User ADMIN_TEST = new User();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final Connection CONNECTION = CONNECTION_SERVICE.getConnection();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION);

    @BeforeClass
    public static void setUp() {
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER_TEST_PASSWORD));
        USER_TEST.setEmail(USER_TEST_EMAIL);
        ADMIN_TEST.setLogin(ADMIN_TEST_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
        ADMIN_TEST.setEmail(ADMIN_TEST_EMAIL);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable User user = USER_REPOSITORY.findOneById(USER_TEST.getId());
        if (user != null) USER_REPOSITORY.remove(user);
    }

    @Before
    public void initDemoData() throws Exception {
        USER_REPOSITORY.add(USER_TEST);
        USER_REPOSITORY.add(ADMIN_TEST);
    }

    @After
    public void clearData() throws Exception {
        USER_REPOSITORY.remove(USER_TEST);
        USER_REPOSITORY.remove(ADMIN_TEST);
    }

    @Test
    public void testFindByLogin() throws Exception {
        @Nullable final User user = USER_REPOSITORY.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertNull(USER_REPOSITORY.findByLogin(null));
    }

    @Test
    public void testFindByEmail() throws Exception {
        @Nullable final User user = USER_REPOSITORY.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertNull(USER_REPOSITORY.findByEmail(null));
    }

    @Test
    public void testIsLoginExist() throws Exception {
        Assert.assertTrue(USER_REPOSITORY.isLoginExist(USER_TEST.getLogin()));
    }

    @Test
    public void testIsEmailExist() throws Exception {
        Assert.assertTrue(USER_REPOSITORY.isEmailExist(USER_TEST.getEmail()));
    }

    @Test
    public void testAddUser() throws Exception {
        @NotNull final User user = new User();
        USER_REPOSITORY.add(user);
        Assert.assertNotNull(USER_REPOSITORY.findOneById(user.getId()));
    }

    @Test
    public void testClear() throws Exception {
        USER_REPOSITORY.clear();
        Assert.assertEquals(0, USER_REPOSITORY.getSize());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<User> users = USER_REPOSITORY.findAll();
        Assert.assertEquals(users.size(), USER_REPOSITORY.getSize());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<User> users = USER_REPOSITORY.findAll();
        @NotNull final User user1 = users.get(0);
        @NotNull final String projectId = user1.getId();
        Assert.assertEquals(user1.getId(), USER_REPOSITORY.findOneById(projectId).getId());
    }

    @Test
    public void testRemove() throws Exception {
        @Nullable final User userRemove = USER_REPOSITORY.remove(ADMIN_TEST);
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(ADMIN_TEST.getId(), userRemove.getId());
        Assert.assertNull(USER_REPOSITORY.findOneById(userRemove.getId()));
    }

    @Test
    public void testRemoveById() throws Exception {
        @Nullable final User userRemove = USER_REPOSITORY.removeById(ADMIN_TEST.getId());
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(ADMIN_TEST.getId(), userRemove.getId());
        Assert.assertNull(USER_REPOSITORY.findOneById(userRemove.getId()));
    }

    @Test
    public void update() throws Exception {
        ADMIN_TEST.setLastName("LAST");
        ADMIN_TEST.setFirstName("FIRST");
        ADMIN_TEST.setMiddleName("MODDLE");
        USER_REPOSITORY.update(ADMIN_TEST);
        Assert.assertEquals(ADMIN_TEST.getLastName(), USER_REPOSITORY.findByLogin(ADMIN_TEST_LOGIN).getLastName());
        Assert.assertEquals(ADMIN_TEST.getFirstName(), USER_REPOSITORY.findByLogin(ADMIN_TEST_LOGIN).getFirstName());
        Assert.assertEquals(ADMIN_TEST.getMiddleName(), USER_REPOSITORY.findByLogin(ADMIN_TEST_LOGIN).getMiddleName());
    }

}