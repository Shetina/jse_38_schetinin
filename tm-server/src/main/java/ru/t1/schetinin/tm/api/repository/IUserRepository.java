package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    Boolean isLoginExist(@NotNull String login) throws Exception;

    Boolean isEmailExist(@NotNull String email) throws Exception;

    void update(@NotNull User user) throws Exception;

}
