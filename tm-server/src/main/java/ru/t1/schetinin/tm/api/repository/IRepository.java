package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void clear() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M add(@NotNull M model) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws Exception;

    int getSize() throws Exception;

    @Nullable
    M remove(@Nullable M model) throws Exception;

    @Nullable
    M removeById(@Nullable String id) throws Exception;

    @Nullable
    M removeByIndex(@Nullable Integer index) throws Exception;

    void removeAll(@Nullable Collection<M> collection) throws Exception;

}