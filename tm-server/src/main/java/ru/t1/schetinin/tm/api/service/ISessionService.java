package ru.t1.schetinin.tm.api.service;

import ru.t1.schetinin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}