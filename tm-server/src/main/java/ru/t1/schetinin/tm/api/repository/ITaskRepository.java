package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Task create(@NotNull String userId, @NotNull String name) throws Exception;

    void update(@NotNull Task task) throws Exception;

}