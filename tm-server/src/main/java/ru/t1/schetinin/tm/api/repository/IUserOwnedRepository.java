package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M add(@NotNull String userId, @NotNull M model) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws Exception;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

}