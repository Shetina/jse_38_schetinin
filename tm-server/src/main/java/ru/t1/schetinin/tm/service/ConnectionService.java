package ru.t1.schetinin.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.IPropertyService;

import java.sql.Connection;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final BasicDataSource dataSource;

    @NotNull
    private final IPropertyService propertyService;


    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.dataSource = dataSource();
    }

    public BasicDataSource dataSource() {
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        final BasicDataSource result = new BasicDataSource();
        result.setUsername(username);
        result.setUrl(url);
        result.setPassword(password);
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}