package ru.t1.schetinin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.repository.ISessionRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.ISessionService;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}