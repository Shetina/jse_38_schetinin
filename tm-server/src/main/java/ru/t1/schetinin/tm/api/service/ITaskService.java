package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) throws Exception;

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    public void updateProjectIdById(@Nullable String userId, @Nullable String id, @Nullable String projectId) throws Exception;

}