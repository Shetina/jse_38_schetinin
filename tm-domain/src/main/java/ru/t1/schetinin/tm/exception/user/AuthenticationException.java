package ru.t1.schetinin.tm.exception.user;

public final class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}
