package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@NotNull final User user) {
        super(user);
    }

}